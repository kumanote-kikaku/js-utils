var pkg = require('../package.json');
var fs = require('fs');
var path = require('path');
var uglifyjs = require('uglify-js');

var baseDir = path.dirname(__dirname);
var srcPath = path.join(baseDir, 'src', 'site.js');
var destPath = path.join(baseDir, 'dist', 'site.js');

var result = uglifyjs.minify(srcPath);
fs.writeFileSync(
  destPath,
  '/*! js-utils ' +
  pkg.version +
  ' | MIT License | https://bitbucket.org/kumanote-kikaku/js-utils */\n' +
  result.code, 'UTF-8');
