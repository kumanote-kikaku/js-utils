// jQuery required
if (typeof window === 'undefined' || typeof window.jQuery === 'undefined') {
  throw 'required jquery.';
}

(function ($, site) {
  'use strict';

  /** windowのjQueryオブジェクト */
  var $win = $(window);

  /** サイズ定義 */
  var sizeDefinitions = [];

  /** サイズ切り替え時のリスナー */
  var sizeListeners = [];

  /** 現在のサイズ名 */
  var sizeCurrent = null;

  /**
   * ウインドウ幅に応じてサイズ名を更新します。<br>
   * サイズが更新された場合はリスナーに通知されます。
   * @private
   */
  function updateSize() {
    var size = null, w = $win.width(), def, l, i, old;
    for (l = sizeDefinitions.length, i = 0; i < l; ++i) {
      def = sizeDefinitions[i];
      if (
        (def.min === null || def.min <= w) &&
        (def.max === null || w <= def.max)) {
        size = def.name;
        break;
      }
    }
    if ((old = sizeCurrent) === size) return;
    sizeCurrent = size;
    for (l = sizeListeners.length, i = 0; i < l; ++i) {
      sizeListeners[i](size, old);
    }
  }

  /**
   * サイズを定義します。
   * @param {array...} definition 定義
   * @param {number|null} definition.min 最小幅 / nullの場合は下限なし
   * @param {number|null} definition.max 最大幅 / nullの場合は上限なし
   * @param {string} definition.name サイズ名
   */
  function setupSizes(/* definition, ... */) {
    var definitions = $.makeArray(arguments), l, i, def;
    for (l = definitions.length, i = 0; i < l; ++i) {
      if ($.isPlainObject(def = definitions[i]) &&
        (typeof def.min === 'number' || def.min === null) &&
        (typeof def.max === 'number' || def.max === null) &&
        (typeof def.name === 'string')) {
        sizeDefinitions.push({
          min: def.min,
          max: def.max,
          name: def.name
        });
      }
    }
    updateSize();
  }

  /**
   * サイズ切り替え時のリスナーを追加します。<br>
   * リスナーには切り替え後のサイズ名と、切り替え前のサイズ名が渡されます。
   * @param {function} listener リスナー
   */
  function addSizeListener(listener) {
    var l, i;
    if (!$.isFunction(listener)) return;
    for (l = sizeListeners.length, i = 0; i < l; ++i) {
      if (sizeListeners[i] === listener) return;
    }
    sizeListeners[l] = listener;
  }

  /**
   * サイズ切り替え時のリスナーを削除します。
   * @param {function} listener リスナー
   */
  function removeSizeListener(listener) {
    var l, i;
    for (l = sizeListeners.length, i = 0; i < l; ++i) {
      if (sizeListeners[i] === listener) {
        sizeListeners.splice(i, 1);
        return;
      }
    }
  }

  /**
   * 現在のサイズ名を取得します。
   * @returns {string|null} サイズ名
   */
  function getCurrentSizeName() {
    return sizeCurrent;
  }

  /**
   * 現在のサイズにあっているかどうかを判定します。<br>
   * 配列での複数指定時は、どれか一つでもあっていれば正と判定します。<br>
   * nullの場合は常に正と判定します。
   * @param {string|array|null} sizeName サイズ名
   * @returns {boolean} 判定結果
   */
  function isMatchSize(sizeName) {
    var l, i;
    if (sizeName === null) return true;
    if (typeof sizeName === 'string') sizeName = [sizeName];
    if (!$.isArray(sizeName)) return false;
    for (l = sizeName.length, i = 0; i < l; ++i) {
      if (sizeName[i] === sizeCurrent) return true;
    }
    return false;
  }

  /**
   * メニュー非表示タイマーを解除します。
   * @private
   */
  function clearMenuHideTimer(menu) {
    if (menu.hideTimer !== null) {
      clearTimeout(menu.hideTimer);
      menu.hideTimer = null;
    }
  }

  /**
   * 子メニューを表示します。
   * @private
   * @param {jQuery} $children 子メニューの要素
   */
  function showMenu(menu, $children) {
    if (menu.$current === $children) return;
    if (menu.$current) {
      menu.$current.stop(true, true).slideUp(menu.duration * 0.8 | 0);
    }
    menu.$current = $children;
    if (menu.$current) {
      menu.$current.stop(true, true).slideDown(menu.duration);
    }
  }

  function keepMenu(menu, $children) {
    if (menu.$current !== $children) return;
    menu.hideTimer = setTimeout(function () {
      menu.$current.stop(true, true).slideUp(menu.duration * 0.8 | 0);
      menu.$current = null;
    }, menu.hideDelay);
  }

  /**
   * プルダウンメニューを設定します。
   * @param {object} options 設定オプション
   * @param {string} [options.itemSelector='.menu-item'] 親メニューのセレクタ
   * @param {string} [options.linkSelector='>a'] 親メニュー内のリンク要素のセレクタ（親メニューからの相対）
   * @param {string} [options.childrenSelector='>ul'] 子メニューのセレクタ（親メニューからの相対）
   * @param {number} [options.duration=200] メニューの表示速度（ミリ秒）
   * @param {number} [options.delay=500] メニュー非表示の遅延時間（ミリ秒）
   * @param {string|array} [options.sizes=[]] メニューを有効化するサイズ名
   */
  function setupMenu(options) {
    var itemSelector = '.menu-item';
    var linkSelector = '>a';
    var childrenSelector = '>ul';
    var menu = {
      duration: 200,
      hideDelay: 500,
      sizes: [],
      hideTimer: null,
      $current: null
    };

    if ($.isPlainObject(options)) {
      if (typeof options.itemSelector === 'string') itemSelector = options.itemSelector;
      if (typeof options.linkSelector === 'string') linkSelector = options.linkSelector;
      if (typeof options.childrenSelector === 'string') childrenSelector = options.childrenSelector;
      if (typeof options.duration === 'number') menu.duration = options.duration;
      if (typeof options.delay === 'number') menu.hideDelay = options.delay;
      if (typeof options.sizes === 'string') menu.sizes = options.sizes;
      if ($.isArray(options.sizes)) menu.sizes = options.sizes.slice();
    }

    $(itemSelector).each(function () {
      var $item = $(this);
      var $link = $item.find(linkSelector);
      var $children = $item.find(childrenSelector);

      $link.hover(function () {
        if (!isMatchSize(menu.sizes)) return;
        clearMenuHideTimer(menu);
        showMenu(menu, $children);
      }, function () {
        if (!isMatchSize(menu.sizes)) return;
        clearMenuHideTimer(menu);
        keepMenu(menu, $children);
      });
      $children.hover(function () {
        if (!isMatchSize(menu.sizes)) return;
        clearMenuHideTimer(menu);
      }, function () {
        if (!isMatchSize(menu.sizes)) return;
        clearMenuHideTimer(menu);
        keepMenu(menu, $children);
      });
      addSizeListener(function () {
        clearMenuHideTimer(menu);
        $children.toggle(!isMatchSize(menu.sizes));
      });
    });
  }

  function setupFixedHeader() {
  }

  /**
   * スクロール時に表示されるページトップボタンを設定します。
   * @param {object} options オプション
   * @param {string|array|null} [options.sizes=null] ページトップボタンを有効化するサイズ名
   * @param {string} options.className ボタン表示時につけるクラス名
   * @param {number} [options.threshold=0] ボタンを表示開始するスクロール位置
   * @param {jQuery} options.element ボタンの要素
   */
  function setupPageTop(options) {
    var sizes = null;
    var className = 'show';
    var threshold = 0;
    var $element = null;

    function update() {
      if (isMatchSize(sizes)) {
        $element.toggleClass(className, $win.scrollTop() > threshold);
      } else {
        $element.removeClass(className);
      }
    }

    if ($.isPlainObject(options)) {
      if (typeof options.sizes === 'string') sizes = [options.sizes];
      if ($.isArray(options.sizes)) sizes = options.sizes.slice();
      if (typeof options.className === 'string') className = options.className;
      if (typeof options.threshold === 'number') threshold = options.threshold;
      if (options.element instanceof $) $element = options.element;
    }

    if ($element === null) {
      return;
    }
    $win.on('scroll resize', update);
    update();
  }

  /**
   * クラス名によるいくつかの機能追加を設定します。<br>
   * <dl>
   * <dt>.scroll</dt>
   * <dd>アンカー先へのスムーススクロール</dd>
   * <dt>.popup</dt>
   * <dd>画像のポップアップ表示</dd>
   * <dt>.popup-iframe</dt>
   * <dd>iframeでのポップアップ表示</dd>
   * <dt>.dotdotdot</dt>
   * <dd>複数行での行末表示</dd>
   * </dl>
   */
  function setupTweak() {
    if ($.smoothScroll) {
      $('.scroll').smoothScroll();
    }
    if ($.magnificPopup) {
      $('.popup-iframe').magnificPopup({
        type: 'iframe'
      });
    }
    if ($.fn.dotdotdot) {
      $('.dotdotdot').each(function () {
        var $element = $(this);
        function update() {
          $element.trigger('destroy');
          $element.dotdotdot();
        }
        addSizeListener(update);
        update();
      });
    }
  }

  $.extend(site, {
    setupSizes: setupSizes,
    addSizeListener: addSizeListener,
    removeSizeListener: removeSizeListener,
    getCurrentSizeName: getCurrentSizeName,
    isMatchSize: isMatchSize,

    setupMenu: setupMenu,

    setupFixedHeader: setupFixedHeader,

    setupPageTop: setupPageTop,

    setupTweak: setupTweak
  });

  $win.on({
    resize: function () {
      updateSize();
    }
  });
})(window.jQuery, window.site = {});
